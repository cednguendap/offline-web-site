/**
 * Source: https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API/Using_Service_Workers
 */
class OfflineWsException extends Error
{
    /**
     * This class just serves displayed in the console different error obtained in the application
     */
	constructor(msg)
	{
		super(msg);
		name=constructor.name;
		if(typeof Error.captureStackTrace==='function')
		{
			Error.captureStackTrace(this,constructor);
		}
		else 
		{
			stack=(new Error(msg)).stack;
		}
	}
}
//Constant used
let cacheName='v1';
let keyStorage="offline-ws-date";
let responseError="error ";
let dbName="offline-ws-db";
let resultDb="";


function log(message)
{
    console.log(`OfflineWs Log: ${message}`);
}
function shouldUpdate() 
{
    /**
     * This function makes sure to check if it is necessary to update the cache
    if the number of days after the last update is greater than 7 then an update is required 
    otherwise the data contained in the cache is used
     */
    let now=new Date();
    let previous=new Date(window.localStorage.getItem(keyStorage));
    let nberDay=new Number((previous.getTime()/86400000)-(now.getTime()/86400000)).toFixed(0);
    return nberDay>=7;
}
function fetchData(request)
{
    //This function allows you to retrieve data from the server and put it in the cache
    fetch(request.clone())
    .then((response)=> 
    {
        caches.open(cacheName)
        .then((cache)=> {
            cache.put(request,response.clone());
            window.localStorage.setItem(keyStorage,Date.now());
        }).catch((error)=> {
            throw new OfflineWsException("Cannot access to cache with name "+cacheName)
        });
    });
}

/*
By default after some time the browser hides its cache and 
if you want your site to no longer be temporarily available offline, 
you must use the following code. 
a dialog box will open asking the user to validate the fact that your site will be permanently 
stored in his machine and can be accessible anytime without internet connection
*/
if (navigator.storage && navigator.storage.persist) 
  navigator.storage.persist().then(function(persistent) {
    if (persistent)
      console.log("Le stockage ne sera pas nettoyé sauf en cas d’action humaine explicite");
    else
      console.log("Le stockage peut être nettoyé par l’UA en cas de pression de stockage.");
  });


  //We handle the event for the installation of the service worker
self.addEventListener("install",(event)=>{    
    event.waitUntil(
        caches.open(cacheName)
        .then((cache)=> {
            cache.addAll([
                '/',
                '/index.js',
                '/style.css',
                '/offline-ws.js',
                '/manifest.json',
                '/icon.png',
                '/index.html',
                '/shortcutHome.html'
            ]);
        }).catch((error)=> {
            throw new OfflineWsException("Cannot access to cache with name "+cacheName)
        }));
        self.ClientRectList.claim();
    log("Successfull installation");
});


/*
We handle event for activation of service worker
We clean any file already present in the cache to ensure that the new version of 
service worker is fully taken into account
*/
self.addEventListener('activate', function(event) {		  
    event.waitUntil(
        caches.keys().then(function(keyList) {
        return Promise.all(keyList.map(function(key) {
            if (cacheName.indexOf(key) === -1) {
            return caches.delete(keyList[i]);
            }
        }));
        })
    );
    log("OfflineWs is activate");
});

/**
 * We manage the event at the request of the user.
when the user sends a method, we check that the time of the last update (> = 7 days) 
if it is necessary to make an update then we make a request to the server and we put update 
the cache and then return the response from the user's request
 */
self.addEventListener('fetch',(event)=>{
    log("Offline-ws make a fetch method");
    if(shouldUpdate()) fetchData(event.request);
    event.respondWith(
    	caches.match(event.request).then((response)=>{
    		if(response) return response;
    	}).catch((error)=>{
    		return new Response(responseError,{headers:{'Content-type':'text/html'}});
    	})
    );
});



/**
 * Add Web App to a User Home Screen
 * Source: https://codelabs.developers.google.com/codelabs/add-to-home-screen/#4,
 *         https://medium.com/@sam20gh/how-to-add-add-to-home-screen-to-your-website-4b07aee02676
 * 
 * 
 * To add the icon of your website to the desktop
  of the user you just have to edit the manifest file and insert it in the header of your web page
Consult the sources available to you for more details
 */

